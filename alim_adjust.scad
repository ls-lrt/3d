$fn = 200;

r1 = 5;
h1 = 8;

r2 = 7;
h2 = 6;

d = 4;
c = (r2 + d) * 2;

difference() {
	translate ([-c/2, -c/2, 0]) cube([c, c, h1 + h2 ], center = false);
	{
		cylinder(h=h2, r=r2);
		translate([0, 0, h2]) cylinder(h=h1, r=r1);
	}
}
