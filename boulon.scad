$fn=200;

module socle(_d)
{
	_h = 0.2;
	cylinder(h = _h, r = _d/2);
}

module tete(d_tete, d_bas, h_tete) {
		cylinder(h_tete/2, d_bas/2, d_tete / 2);
		translate([0, 0, h_tete/2]) cylinder(h=h_tete/2, r=d_tete/2);	
}

module vis(_d, _h) {
	_r = _d/2;
	d_tete = 10;
	h_tete = 5;
	r_filetage = 1;
	translate([0, 0, _h]) {
		difference() {
			tete(d_tete, _d, h_tete);
			translate([0, 0, h_tete - 1]) cube([2, d_tete, 2], center = true);
		}
	}
	
	cylinder(h = _h, r=_r);
	linear_extrude(height = _h, center = false, convexity = 10, twist = 180 * _h )
	translate([_r - r_filetage, 0, 0]) square(size = 1.5 * r_filetage);
}

module _filetage(r_int, r_filetage, _h) {
		linear_extrude(height = _h, center = false, convexity = 10, twist = 180 * _h )
		translate([r_int - r_filetage, 0, 0]) square(size = 1.5 * r_filetage);
}

module filetage(a, b, c) {
	_filetage(a, b, c);
	for (i = [0:10])
		translate([0, 0, i/40]) _filetage(a, b, c);
}

module boulon(d_int, d_ext, _h) {
	r_int = d_int/2;
	r_ext = d_ext/2;
	r_filetage = 1.2;

	difference() {
		difference() {
			translate([0, 0, 1])cylinder(h = _h-2, r = r_ext, $fn=8);
			translate([0, 0, 0]) cylinder(h = _h, r=r_int);
		}
		filetage(r_int, r_filetage, _h);
	}
}


boulon(5.1, 10, 6);
