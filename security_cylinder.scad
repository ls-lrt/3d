$fn = 200;

r_int  = 31.7 / 2;
h_int1 = 57.1;
h_int2 = 25.4;
e      = 2.5;

module prism(l, w, h){
       polyhedron(
               points=[[0,0,0], [l,0,0], [l,w,0], [0,w,0], [0,w,h], [l,w,h]],
               faces=[[0,1,2,3],[5,4,3,2],[0,4,5,1],[0,3,4],[5,2,1]]
               );
}

module security_cylinder() {
	difference() {
		cylinder(h=h_int1 + e, r = r_int + e);
		translate([0, 0, e]) {
			difference() {
  				cylinder(r=r_int, h=(h_int1 + e));
  				translate([-r_int, -r_int, 0]) prism(2*r_int, 2*r_int, h_int1 - h_int2);
			}
		}
	}
}

security_cylinder();
