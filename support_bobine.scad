$fn = 200;

module axe() {
	cylinder(r = 35, h = 7);
	translate([0, 0, 7]) cylinder(r = 25, h = 50);
}

module support_moteur() {
	translate([-2.5, -25, 0]) {
		cube([5, 50, 6], center = false);
		translate([0, 9, 6]) cube([5, 8, 6], center = false);
		translate([0, 32, 6]) cube([5, 8, 6], center = false);
	}
}

difference() {
	axe();
	support_moteur();
	translate([16, 22, 0]) cylinder (r = 4, h = 3);
	translate([16, -22, 0]) cylinder (r = 4, h = 3);
	translate([0, 0, 15]) cylinder(r = 15, h = 42);
}
