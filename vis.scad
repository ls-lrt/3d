$fn=200;

module socle(_d)
{
	_h = 0.2;
	cylinder(h = _h, r = _d/2);
}

module tete(d_tete, d_bas, h_tete) {
		cylinder(h_tete/2, d_bas/2, d_tete / 2);
		translate([0, 0, h_tete/2]) cylinder(h=h_tete/2, r=d_tete/2);	
}

module vis(_d, _h) {
	_r = _d/2;
	d_tete = 10;
	h_tete = 5;
	r_filetage = 1;
	translate([0, 0, _h]) {
		difference() {
			tete(d_tete, _d, h_tete);
			translate([0, 0, h_tete - 1]) cube([2, d_tete, 2], center = true);
		}
	}

	cylinder(h = _h, r=_r);
	linear_extrude(height = _h, center = false, convexity = 10, twist = 180 * _h )
	translate([_r - r_filetage, 0, 0]) square(size = 1.5 * r_filetage);
}

translate([0,0,0.2])vis(5, 30);
