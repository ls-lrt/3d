
module booster(t) {
	rotate_extrude() {
		difference() {
			rotate([0,0,90])
			translate([5*t,0])
			scale([2,1])
			difference() {
				circle(d=20*t);
				translate([-10*t, 0])square([20*t,20*t]);
			}
		translate([0,-20*t])square([20*t,20*t]);
		}
		difference() {
			translate([0, -5*t]){square([10*t,10*t]);
			circle(r=10*t);}
			translate([-10*t, -20*t])square([10*t,30*t]);
			translate([0,-15*t])square([10*t,1*t]);
		}
	}
}

module ailette(t) {
	difference() {
		scale([3,1,3])rotate_extrude()
		translate([90*t, 0, 0])scale([2,.5])circle(d=25*t, $fn=100);
		translate([-400*t,0,-20*t])cube([800*t,200*t,40*t]);
		translate([0,0,0])rotate([0,0,-30])translate([-400*t,-240*t,-20*t])cube([800*t,200*t,40*t]);
	}
}

module aile(t) {
	ailette(2*t);
	translate([5*t,-210*t,0*t])rotate([0,90,0])booster(10*t);
}

module fuselage(t) {
	translate([0, -100*t, 0])aile(t);
	rotate([120, 0, 0])translate([0, -100*t, 0])aile(t);
	rotate([-120, 0, 0])translate([0, -100*t, 0])aile(t);
}

module corp(t) {
	rotate([0, 90, 0])rotate_extrude() {
		rotate([0,0,-90])
		difference() {
			translate([0,1*t])scale([5,1])circle(d=10*t, $fn=200);
			translate([-25*t,0])square([50*t,10*t]);
			//#translate([18*t,-5*t])square([7*t,10*t]);
			translate([24*t,t])circle(r=7*t);
		}
	}
}

module fusee(t) {
	fuselage(2*t);
	translate([2000*t,0,0])corp(80*t);
}

module suport(t) {
	difference() {
		cylinder(r=50*t, h=90);
		cylinder(r=40*t, h=90);
	}
}

suport(0.4);
translate([0,0,27])rotate([0, -90, 0])fusee(0.1);
